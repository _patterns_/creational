# Фабричный метод

> Абстрактная фабрика — это порождающий паттерн проектирования, который позволяет создавать семейства связанных объектов, не привязываясь к конкретным классам создаваемых объектов.

## Проблема

> Cоздание объектов продуктов так, чтобы они сочетались с другими продуктами того же семейства.

## Решение

> 1. Выделить общие интерфейсы для отдельных продуктов, составляющих семейства.
> 2. Cоздать абстрактную фабрику — общий интерфейс, который содержит методы создания всех продуктов семейства.

## Применимость

1. Когда бизнес-логика программы должна работать с разными видами связанных друг с другом продуктов, не завися от конкретных классов продуктов.

## Шаги реализация

1. Создайте таблицу соотношений типов продуктов к вариациям семейств продуктов.

2. Сведите все вариации продуктов к общим интерфейсам.

3. Определите интерфейс абстрактной фабрики. Он должен иметь фабричные методы для создания каждого из типов продуктов.

4. Создайте классы конкретных фабрик, реализовав интерфейс абстрактной фабрики. Этих классов должно быть столько же, сколько и вариаций семейств продуктов.

5. Измените код инициализации программы так, чтобы она создавала определённую фабрику и передавала её в клиентский код.

6. Замените в клиентском коде участки создания продуктов через конструктор вызовами соответствующих методов фабрики.

## Ссылки на реализации

- ### [TypeScript](https://gitlab.com/_javascript_/patterns/-/tree/master/abstract-factory/index.ts)
- ### [Rust]()

## Ссылка на источник

- [refactoring.guru](https://refactoring.guru/ru/design-patterns/factory-method)
